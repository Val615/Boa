using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class DamageNumber : MonoBehaviour
{
    public TextMeshPro damageNumber;
    public float lifespan = 0.75f;
    public float fadeLength = 0.75f;

    public void Update()
    {
        if (lifespan < 0)
        {
            transform.DOScale(0, fadeLength).OnComplete(DestroyObject);
        }
        else
        {
            lifespan -= Time.deltaTime;
        }
    }
    public void Init(int damage)
    {
        Vector3 lookAtCam = Camera.main.transform.position - this.transform.position;
        transform.rotation = Quaternion.LookRotation(-lookAtCam);
        damageNumber.text = damage.ToString();
        float range = 1.5f;
        Vector3 originalVector = transform.up + transform.right;
        float x = originalVector.x + Random.Range(1, range);
        float y = originalVector.y + Random.Range(1, range);
        float z = originalVector.z + Random.Range(1, range);


        Vector3 newVector = new Vector3(x,y,z); 
        GetComponent<Rigidbody>().AddForce(newVector * 40);
    }
    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}
