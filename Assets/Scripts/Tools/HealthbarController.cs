using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text;

public class HealthbarController : MonoBehaviour
{
    public TextMeshProUGUI healthText;
    public Slider healthSlider;

    public void UpdateHealth(float h)
    {
        healthSlider.value = h / GameManager.instance.maxHealth;
        Debug.LogFormat("{0} / {1}", h, GameManager.instance.maxHealth);
        healthText.text =  h + " / " + GameManager.instance.maxHealth;
    }
}
