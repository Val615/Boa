using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDummy : MonoBehaviour
{
    public int health = 100;
    public int armor = 10;
    public TestCrateSpawner spawner;
    public GameObject damageNumberPrefab;
    public void TakeDamage(int damage)
    {
        Debug.LogFormat("{0}, {1}", damage, Mathf.Abs(armor / 3));
        var totalDamage = damage - Mathf.Abs(armor / 3);
        health -= totalDamage;
        GameObject iDamageNumber = Instantiate(damageNumberPrefab, transform.position, transform.rotation);
        iDamageNumber.GetComponent<DamageNumber>().Init(totalDamage);
        Debug.LogFormat("New health: {0}", health);
        if (health < 1) { Die(); }
    }

    public void Die()
    {
        transform.DOScale(Vector3.zero, 1f).OnComplete(removeObject);
    }

    public void removeObject()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (spawner != null)
        {
            spawner.InstantiateCrate();
        }
    }

}
