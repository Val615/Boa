using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Advisory notice: We may not even need this lol

public class DamageOverTime : MonoBehaviour
{
    public DamageOverTimeSettings dots;
    public NPC target;

    public virtual void Start()
    {
        if (target == null)
        {
            target = GetComponentInChildren<NPC>();
        }
        ApplyDamageOverTime();
    }

    public virtual void Update()
    {

    }
    private IEnumerator ApplyDamageOverTime()
    {
        for (int i = 0; i < dots.dStartingTicks; i++)
        {
            Debug.LogFormat("Damage Over Time {0} on {1}", dots.name, target.name);
            yield return new WaitForSeconds(1f);  // Wait for one second before the next tick
        }
    }

    public virtual void Effect()
    {

    }
}
