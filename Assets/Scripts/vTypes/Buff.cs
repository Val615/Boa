using JetBrains.Annotations;
using NUnit.Framework.Interfaces;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;

public class Buff : MonoBehaviour
{
    public NPC target;
   
    public virtual void Start()
    {
        target = GetComponent<NPC>();        
    }

    public virtual void Update()
    {
        
    }

    public virtual void Effect()
    {

    }
}
