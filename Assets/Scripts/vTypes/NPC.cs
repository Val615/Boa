using JetBrains.Annotations;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using static UnityEngine.GraphicsBuffer;

[RequireComponent(typeof(Animator))]
public class NPC : MonoBehaviour
{
    // Stats
    public int maxHealth = 10;
    public int health;
    public int stamina = 10;
    public int mana = 10;
    public int moveSpeed = 40;

    // Flags
    public bool isHostile = false;
    public bool isEnemy = false;
    public bool isAlive = true;

    // Ability
    public bool canMove = true;
    public bool isMoving = false;
    public bool canJump = true;
    public bool canAttack = true;
    public bool isAttacking = false;
    public bool canCast = true;
    public bool isCasting = false;

    // Components
    public Animator animator;
    public DamageNumber dn;

    // Experimental
    public List<Buff> buffs = new List<Buff>();

    private Dictionary<string, Coroutine> damageOverTime = new Dictionary<string, Coroutine>();
    private Dictionary<string, Coroutine> healOverTime = new Dictionary<string, Coroutine>();

    public virtual void Start()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
        health = maxHealth;
    }

    public virtual void Update()
    {

    }

    public virtual void ApplyBuff(Buff b)
    {

    }

    public virtual void RemoveBuff(Buff b)
    {

    }

    public virtual void AddDot(DamageOverTimeSettings dot, string key)
    {
        Debug.Log("AddDot triggered");
        StartCoroutineWithKey(key, DamageOverTime(dot));
    }

    public virtual void AddHot()
    {

    }

    private void StartCoroutineWithKey(string key, IEnumerator coroutine)
    {
        damageOverTime[key] = StartCoroutine(coroutine);
    }

    private void StopCoroutineByKey(string key)
    {
        if (damageOverTime.ContainsKey(key))
        {
            StopCoroutine(damageOverTime[key]);
            damageOverTime.Remove(key);
        }
    }

    public virtual void RemoveDot(DamageOverTimeSettings dot, string key)
    {
        if(damageOverTime.ContainsKey(key))
        {
            StopCoroutineByKey(key);
        }
    }

    private IEnumerator DamageOverTime(DamageOverTimeSettings dots)
    {
        for (int i = 0; i < dots.dStartingTicks; i++)
        {
            Debug.LogFormat("Damage Over Time {0} on {1} for {2} damage", dots.name, this.name, dots.damage);
            if (dots.isHealing)
            {
                Heal(dots.damage);
            }
            else
            {
                TakeDamage(dots.damage);
            }
            yield return new WaitForSeconds(1f);  // Wait for one second before the next tick
        }
    }

    // The mechanics for taking damage, dots, buffs, etc. should all be HERE!

    public virtual void TakeDamage(int d)
    {
        if (health <= 0)
        {
            return;
        }
        Debug.LogFormat("Damage taken: {0}", d);
        health -= d;
        ThrowDamage(d);
        if (health >= 1)
        {
            animator.SetTrigger("Take Damage");
            return;
        }
        Die();
    }

    public void ThrowDamage(int d)
    {
        DamageNumber damageNumber = Instantiate(dn, transform.position, transform.rotation);
        damageNumber.Init(d);
    }

    public virtual void Heal(int h)
    {
        if (health < maxHealth && isAlive)
        {
            Debug.LogFormat("Healed for {0} health", h);
            ThrowDamage(h);
            health = +h;
        }
    }

    public virtual void Die()
    {
        isAlive = false;
        health = 0;
        animator.SetTrigger("Die");
        //GetComponentInChildren<Collider>().enabled = false;
        var colliders = GetComponentsInChildren<Collider>();
        foreach (Collider c in colliders)
        {
            c.enabled = false;
        }

    }
    public virtual void Attack()
    {

    }

    public virtual void Cast()
    {

    }
}
