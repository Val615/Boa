using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentalHazard : MonoBehaviour
{
    public int damage;
    public float frequency;
    public bool isRepeated;
    public List<NPC> victims = new List<NPC>();
    public DamageOverTimeSettings dotSettings;
    private DamageOverTime dot;

    public string id;

    public void Start()
    {
        dot = GetComponent<DamageOverTime>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInChildren<NPC>())
        {
            Debug.LogFormat("{0} entered {1}, an environmental hazard", other.name, this.name);
            victims.Add(other.GetComponent<NPC>());
            if (isRepeated)
            {
                dotSettings.dRemainingTicks = dotSettings.dStartingTicks = 99;
            }
            //other.GetComponentInChildren<NPC>().dots.Add(dot);
            other.GetComponentInChildren<NPC>().AddDot(dotSettings, other.name);
        }
        else
        {
            Debug.LogFormat("{0} entered collosion but wasn't properly tracked", other.name);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.GetComponentInChildren<NPC>())
        {
            Debug.LogFormat("{0} exited {1}, an environmental hazard", other.name, this.name);
            //other.GetComponentInChildren<NPC>().dots.Remove(dot);
            other.GetComponentInChildren<NPC>().RemoveDot(dotSettings, other.name);
            victims.Remove(other.GetComponent<NPC>());
        }
    }
}
