using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TestDummyMk2 : NPC
{
    public int armor = 3;
    public TestCrateSpawner spawner;
    public Rigidbody rb;

    public override void Start()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody>();
        }
    }
    public void Update()
    {
        
    }

    public override void TakeDamage(int d)
    {
        if (health > 0)
        {
            health -= d;
            ThrowDamage(d);
            if (health < 1)
            {
                Die();
            }
        }
    }

    public override void Heal(int h)
    {
        health += h;
    }

    public override void Die()
    {
        rb.useGravity = false;
        rb.mass = 0.2f;
        transform.DOScale(0, 1).OnComplete(SelfDestruct);
    }

    public override void Attack()
    {

    }

    public override void Cast()
    {
        // TODO:    A lot lol. There really should be a neat table of spells
        //          that will be accessed at some point
    }

    public void SelfDestruct()
    {
        spawner.InstantiateCrate();
        Destroy(gameObject);
    }

}
