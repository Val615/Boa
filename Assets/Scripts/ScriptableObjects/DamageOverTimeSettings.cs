using UnityEngine;

[CreateAssetMenu(fileName = "New DamageOverTimeSettings", menuName = "Damage Over Time Settings")]
public class DamageOverTimeSettings : ScriptableObject
{
    public string dName;
    public int dStartingTicks;
    public int dRemainingTicks;
    public int damage;
    public bool isHealing;
}
