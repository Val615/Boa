using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWeapon : MonoBehaviour
{
    public float collisionDelay;
    public int attackPower;
    public bool hasStruck;

    private void OnTriggerEnter(Collider other)
    {
        Debug.LogFormat("Collision: {0}", other.name);
        if (other.GetComponent<NPC>() && hasStruck == false)
        {
            hasStruck = true;
            other.GetComponent<NPC>().TakeDamage(attackPower);
            if (other.GetComponent<TestDummyMk2>())
            {
                var direction = (this.GetComponent<Rigidbody>().position - other.transform.position).normalized;
                other.GetComponent<Rigidbody>().AddForce(-direction * 2f, ForceMode.Impulse);
            }

        }
    }
}
