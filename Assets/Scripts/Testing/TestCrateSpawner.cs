using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCrateSpawner : MonoBehaviour
{
    public GameObject crate;

    public void Start()
    {
        if (crate != null)
        {
            InstantiateCrate();
        }
    }

    public void InstantiateCrate()
    {
        GameObject instantiatedCrate = Instantiate(crate, transform.position, transform.rotation);
        instantiatedCrate.GetComponent<TestDummyMk2>().spawner = this;
    }
}
